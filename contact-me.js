// TODO

const selectEl = document.getElementById('contact-kind');

const setSelectValidity = function () {
    if (selectEl.value === 'job-opportunity') {
        document.querySelector('.job-opportunity').classList.remove('hide');
        document.querySelector('.company-website').classList.remove('hide');
        document.querySelector('.coding-language').classList.add('hide');
    } else {
        document.querySelector('.job-opportunity').classList.add('hide');
        document.querySelector('.company-website').classList.add('hide');
        document.querySelector('.coding-language').classList.remove('hide');
    }
}
selectEl.addEventListener('change', setSelectValidity);

const form = document.getElementById('connect-form');
form.addEventListener('submit', function(e) {
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const message = document.getElementById('message').value;
    if (name.length < 3) {
        e.preventDefault();
        document.querySelector('.name').classList.remove('hide');       
    } else {
        document.querySelector('.name').classList.add('hide');
    }
    if (message.length < 10) {
        e.preventDefault();
        document.querySelector('.message').classList.remove('hide');      
    } else {
        document.querySelector('.message').classList.add('hide');
    }
    if (/\w+@\w+\.\w+/.test(email) !== true) {
        e.preventDefault();
        document.querySelector('.email').classList.remove('hide'); 
    } else {
        document.querySelector('.email').classList.add('hide');
    }
});
