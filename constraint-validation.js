// TODO
const selectEl = document.getElementById('contact-kind');

const setSelectValidity = function () {
    if (selectEl.value ==="choose") {
        selectEl.setCustomValidity("Must select an option");
    } else {
        selectEl.setCustomValidity('');
    }
}

setSelectValidity();
selectEl.addEventListener('change', setSelectValidity);

const form = document.querySelector('form');
form.addEventListener('submit', function(e) {
    const inputs = document.getElementsByClassName('validate-input');
    let formIsValid = true;

    Array.from(inputs).forEach(input => {
        const small = input.parentElement.querySelector('small');
        if (!input.checkValidity()) {
            formIsValid = false;
            small.innerText = input.validationMessage;
        } else {
            small.innerText = '';
        }
    })
if (!formIsValid) {
    e.preventDefault();
}
});